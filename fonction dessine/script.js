//variables pour un niveau
var tailleGrille = [30, 15];
var menu = [];
const originalData = {menu};
var tailleFenetre = [window.innerWidth, window.innerHeight];
var tailleCaseEnPx;
var context;
var canvas;
var leftClickOn = false;
var rightClickOn = false;

function cree_abonnements() {

    canvas = document.getElementById('zoneCanvas');   
    context = canvas.getContext('2d');
    document.oncontextmenu = new Function("return false");

    canvas.addEventListener("mousedown", e => {
        rect = canvas.getBoundingClientRect();
        var X = e.clientX - rect.left-10;
        var Y = e.clientY - rect.top-10;
        if (e.button == 0){
            placeBlock(X, Y);
            leftClickOn = true;
        }
        if (e.button == 2){
            removeBlock(X, Y);
            rightClickOn = true;
        }
    });

    canvas.addEventListener("mouseup", e => {
        leftClickOn = false;
        rightClickOn = false;
    });

    canvas.addEventListener("mousemove", e => {
        if (leftClickOn == true){
            rect = canvas.getBoundingClientRect();
            var X = e.clientX - rect.left-10;
            var Y = e.clientY - rect.top-10;
            placeBlock(X, Y);
        }
        if (rightClickOn == true){
            rect = canvas.getBoundingClientRect();
            var X = e.clientX - rect.left-10;
            var Y = e.clientY - rect.top-10;
            removeBlock(X, Y);
        }
    });

    placeCanvas();
    drawMenu();  

    var button = document.getElementById('save');
    button.addEventListener('click', function (e) {
        button.href = URL.createObjectURL(new Blob([JSON.stringify(originalData)], {
            type: "text/json"
        }));
    });

}

function placeBlock(X, Y){
    var blockAlreadyExist = false;
        var XX = Math.floor(X/tailleCaseEnPx);
        var YY = Math.floor(Y/tailleCaseEnPx);
    for (var i=0; i<menu.length; i++){
        if (menu[i][0]==XX && (menu[i][1])==YY){
            blockAlreadyExist = true;
        }
    }
    if (blockAlreadyExist == false){
        menu.push ([XX,YY]);
    }
    drawMenu();
}

function removeBlock(X, Y){
    var XX = Math.floor(X/tailleCaseEnPx);
    var YY = Math.floor(Y/tailleCaseEnPx);
    for (var i=0; i<menu.length; i++){
        if (menu[i][0]==XX && (menu[i][1])==YY){
            menu.splice(i,1);
        }
    }
    drawMenu();
}
function placeCanvas(){
    var rapprtLargeurHauteurFenetre = tailleFenetre[0]/tailleFenetre[1];
    var rapprtLargeurHauteurGrilleJeu = tailleGrille[0]/tailleGrille[1];
    if (rapprtLargeurHauteurFenetre<rapprtLargeurHauteurGrilleJeu){
        tailleCaseEnPx = (tailleFenetre[0]-60)/tailleGrille[0]
    }
    else{
        tailleCaseEnPx = (tailleFenetre[1]-60)/tailleGrille[1]
    }
    canvas.width = tailleCaseEnPx*tailleGrille[0];
    canvas.height = tailleCaseEnPx*tailleGrille[1];    
}

function drawMenu(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    for (var i = 0; i<tailleGrille[0]; i++){
        for (var j = 0; j<tailleGrille[1]; j++){
            context.strokeStyle = ((i+1)%10==0) ? "blue" : "black";
            context.strokeRect(i*tailleCaseEnPx, j*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);
        }
    }
    for(var i = 0; i<menu.length; i++){
        context.fillStyle = "grey";
        context.strokeStyle = "black";
        var debutRectX = menu[i][0]*tailleCaseEnPx;
        var debutRectY = menu[i][1]*tailleCaseEnPx;
        context.fillRect(debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx);
        context.strokeRect(debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx);
    }
}

window.addEventListener("load", cree_abonnements);
