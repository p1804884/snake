//variables pour un niveau
var tailleGrille;
var delay;
var walls;
var food;
var snake;

//on met en place le cannevas
var canvas;
var context;
var tailleFenetre = [window.innerWidth, window.innerHeight];
var tailleCaseEnPx;

//serpent
var direc;
var game;
var score = 0;

//audio
var audio;
var audioGame = new Audio("./game.mp3");
var currentMusic = false;
var currentMusicVolume = 0;

var levels;
var snakeMenuInterval;
var soundGameMax=0;
var isGame = false;
var pause = false;

var startMenuFirstTime=false;
var gameOverStatut = false;
var direcBloc = false;

var img = new Image();
img.src = "wall.bmp";

var apple = new Image();
apple.src = "apple.bmp";

var oilAsset = new Image();
oilAsset.src = "oil.bmp";

var iceAsset = new Image();
iceAsset.src = "ice.bmp";

var audioGamePictureMax = new Image();
audioGamePictureMax.src = "sonMax.bmp";

var audioGamePictureMedium = new Image();
audioGamePictureMedium.src = "sonMedium.bmp";

var audioGamePictureMute = new Image();
audioGamePictureMute.src = "sonMute.bmp";


var snakeBodyPicture = new Image();
snakeBodyPicture.src = "snakeBody.bmp";

var snakeHeadPictureRight = new Image();
snakeHeadPictureRight.src = "snakeHeadRight.bmp";
var snakeHeadPictureUp = new Image();
snakeHeadPictureUp.src = "snakeHeadUp.bmp";
var snakeHeadPictureLeft = new Image();
snakeHeadPictureLeft.src = "snakeHeadLeft.bmp";
var snakeHeadPictureDown = new Image();
snakeHeadPictureDown.src = "snakeHeadDown.bmp";

var imagesScore = [];// On charge les 9 images pour affiché le score





function cree_abonnements() {
    //initialisation des variables canevas et context
    canvas = document.getElementById('zoneCanvas');
    context = canvas.getContext('2d');
    document.addEventListener("keydown", direction);//ecoute les entrees clavier
    loadMenu();//chargemant du menu
    for(var i=0; i<10; i++){
        var score = new Image();
        score.src = "score"+i+".bmp";
        imagesScore.push(score);
    }
}

function startGame(){
    placeCanvas();
    game = setInterval(playGame,delay);
    drawWall();
    
    soundGame(tailleGrille[0]-1,tailleGrille[1]-1,1);//met en place l'icone de son en bas a droite
}


function placeCanvas(){//
    var rapprtLargeurHauteurFenetre = tailleFenetre[0]/tailleFenetre[1];
    var rapprtLargeurHauteurGrilleJeu = tailleGrille[0]/tailleGrille[1];
    if (rapprtLargeurHauteurFenetre<rapprtLargeurHauteurGrilleJeu){
        tailleCaseEnPx = (tailleFenetre[0]-60)/tailleGrille[0]
    }
    else{
        tailleCaseEnPx = (tailleFenetre[1]-60)/tailleGrille[1]
    }
    canvas.width = tailleCaseEnPx*tailleGrille[0];
    canvas.height = tailleCaseEnPx*tailleGrille[1];
}

function playGame(){ //lance une partie
    moveSnake();
    checkfood();
    
    if(checkColision()==false)
        drawGame();
    else {
        audioGame.pause();
        audioGame.currentTime = 0;
        audio = new Audio("./game_over.mp3");
        audio.play();
        audio.volume = 0.2;
        loadGameOver();
    }
    
}

function drawGame(){//dessine les elements du jeu
    drawIce();
    drawOil();
    drawSnake();
    drawFood();
    drawSong(tailleGrille[0]-1,tailleGrille[1]-1,1);
    drawScore();
     
}

function drawScore(){//dessine le score en haut à droite
    
    if (score<=9)
        context.drawImage(imagesScore[score],(tailleGrille[0]-1.5)*tailleCaseEnPx,0.5*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);
    if (score>9 && score<=99){

        context.drawImage(imagesScore[Math.floor(score/10)],(tailleGrille[0]-3.5)*tailleCaseEnPx,0.5*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);
        context.drawImage(imagesScore[score%10],(tailleGrille[0]-1.5)*tailleCaseEnPx,0.5*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);
    }
}
function drawSong(x, y, taille){//dessine le son en bas à droite
    
    if (currentMusicVolume == 0){
        context.drawImage(audioGamePictureMute,x*tailleCaseEnPx, y*tailleCaseEnPx, tailleCaseEnPx*taille, tailleCaseEnPx*taille);
    }
    else if (currentMusicVolume == 1){
        context.drawImage(audioGamePictureMedium,x*tailleCaseEnPx, y*tailleCaseEnPx, tailleCaseEnPx*taille, tailleCaseEnPx*taille);
    }
    else if (currentMusicVolume == 2){
        context.drawImage(audioGamePictureMax,x*tailleCaseEnPx, y*tailleCaseEnPx, tailleCaseEnPx*taille, tailleCaseEnPx*taille);
    }
}

function soundGame(x,y, taille){//met en place le fonctionnement et le changement du son
    if (soundGameMax <=1){
        document.addEventListener("mousedown", e => {
            rect = canvas.getBoundingClientRect();
            var X = e.clientX - rect.left-10;
            var Y = e.clientY - rect.top-10;

            var xx = Math.floor(X/tailleCaseEnPx);
            var yy = Math.floor(Y/tailleCaseEnPx);
            
            if (xx==x && yy==y){
                context.fillStyle = "#1f2020";
                context.clearRect(x*tailleCaseEnPx, y*tailleCaseEnPx, tailleCaseEnPx*taille, tailleCaseEnPx*taille);
                
                if (currentMusicVolume == 0){
                    if (currentMusic == false){
                        audioGame.play();
                        audioGame.loop = true;
                        audioGame.volume = 0.05;
                        currentMusic = true;
                        currentMusicVolume=1;
                    }
                    audioGame.volume = 0.05;
                    currentMusicVolume = 1;
                }
                else if (currentMusicVolume == 1){
                    audioGame.volume = 0.2;
                    currentMusicVolume = 2;
                }
                else if (currentMusicVolume == 2){
                    audioGame.volume = 0;
                    currentMusicVolume = 0;
                }
                
            }
        });
        soundGameMax+=1;
    }
    
}

function drawSnake(){ //dessine le serpent
    for(var i = 0; i<snake.length; i++){
        var debutRectX = snake[i][0]*tailleCaseEnPx;
        var debutRectY = snake[i][1]*tailleCaseEnPx;
        if(getCurrentDirection() =="gauche"){
            (i==0)?context.drawImage(snakeHeadPictureLeft,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx) :
             context.drawImage(snakeBodyPicture,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx);
        }
        else if(getCurrentDirection() =="haut"){
            (i==0)?context.drawImage(snakeHeadPictureUp,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx) :
             context.drawImage(snakeBodyPicture,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx);
        }
        else if(getCurrentDirection() =="droite"){
            (i==0)?context.drawImage(snakeHeadPictureRight,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx) :
             context.drawImage(snakeBodyPicture,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx);
        }
        else if(getCurrentDirection() == "bas"){
            (i==0)?context.drawImage(snakeHeadPictureDown,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx) :
             context.drawImage(snakeBodyPicture,debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx);
        }
    } 
}

function moveSnake(){//deplace le serpent
    var debutRectX = snake[snake.length-1][0]*tailleCaseEnPx;
    var debutRectY = snake[snake.length-1][1]*tailleCaseEnPx;
    context.clearRect(debutRectX, debutRectY, tailleCaseEnPx, tailleCaseEnPx); //supprime la derniere case du serpent

    if (direc!= null){
        for(var i = snake.length-1; i>0; i--){
            snake[i][0] = snake[i-1][0];
            snake[i][1] = snake[i-1][1];
        }
    }
    if(direc =="gauche"){
        snake[0][0] -= 1;
    }
    else if(direc =="haut"){
        snake[0][1]-= 1;
    }
    else if(direc =="droite"){
        snake[0][0]+= 1;
    }
    else if(direc == "bas"){
        snake[0][1]+= 1;
    }
}

function drawFood(){ //dessine la nourriture
    context.drawImage(apple,food[0][0]*tailleCaseEnPx, food[0][1]*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);
}


function checkfood(){//verifie si on mange une pomme pour le score
    if(snake[0][0]==food[0][0]&&snake[0][1]==food[0][1]){
        var isPosPommeValid = false;
        while (isPosPommeValid == false){
            food[0][0] = Math.floor(Math.random() * (tailleGrille[0]));//place la nouvelle pomme aleatoirement
            food[0][1] = Math.floor(Math.random() * (tailleGrille[1]));
            isPosPommeValid=true;
            for(var i = 0; i<snake.length && isPosPommeValid==true; i++){ //evite l'apparition des pommes sur le corps du snake
                if(snake[i][0]==food[0][0]&&snake[i][1]==food[0][1]){
                    isPosPommeValid = false;
                }
            }
            for(var i = 0; i<walls.length && isPosPommeValid==true; i++){ //evite l'apparition des pommes sur les murs
                if(walls[i][0]==food[0][0]&&walls[i][1]==food[0][1]){
                    isPosPommeValid = false;
                }
            }                
        }
        audio = new Audio("./pomme.mp3");
        audio.play();
        score+=1;

        context.clearRect((tailleGrille[0]-3.5)*tailleCaseEnPx,0.5*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);
        context.clearRect((tailleGrille[0]-1.5)*tailleCaseEnPx, 0.5*tailleCaseEnPx, tailleCaseEnPx, tailleCaseEnPx);

        var nbCaseRajout = Math.floor(Math.random()*2)+1;
        for(var i=0; i<nbCaseRajout;i++){
            snake.push([snake[snake.length-1][0],snake[snake.length-1][1]]);
        }
    }
}

function drawWall(){
    for(var i = 0; i<walls.length; i++){
        var debutRectX = walls[i][0]*tailleCaseEnPx;
        var debutRectY = walls[i][1]*tailleCaseEnPx;
        context.drawImage(img, debutRectX,debutRectY, tailleCaseEnPx, tailleCaseEnPx);
    }
}

function drawIce(){
    for(var i = 0; i<ice.length; i++){
        var debutRectX = ice[i][0]*tailleCaseEnPx;
        var debutRectY = ice[i][1]*tailleCaseEnPx;
        context.drawImage(iceAsset,debutRectX,debutRectY, tailleCaseEnPx, tailleCaseEnPx);
    }
}

function drawOil(){
    for(var i = 0; i<oil.length; i++){
        var debutRectX = oil[i][0]*tailleCaseEnPx;
        var debutRectY = oil[i][1]*tailleCaseEnPx;
        context.drawImage(oilAsset,debutRectX,debutRectY, tailleCaseEnPx, tailleCaseEnPx);
    }
}


function checkColision(){
    //bord de la map
    if(snake[0][0]<0 || snake[0][1]<0 || snake[0][0]>=tailleGrille[0] || snake[0][1]>=tailleGrille[1]){
        
        clearInterval(game);
        direc = null;
        return true;
    }
    //lui meme
    for(var i = 1; i<snake.length; i++){
        if (snake[0][0]==snake[i][0] && snake[0][1]==snake[i][1]){
            
            clearInterval(game);
            direc = null;
            return true;
        }
    }
    //les murs
    for(var i = 0; i<walls.length; i++){
        if (snake[0][0]==walls[i][0] && snake[0][1]==walls[i][1]){
         
            clearInterval(game);
            direc = null;
            return true;
        }
    }

    for(var i = 0; i<oil.length; i++){
        if (snake[0][0]==oil[i][0] && snake[0][1]==oil[i][1]){
         
            if(getCurrentDirection() =="droite"){
                direc ="haut";
            }
            else if(getCurrentDirection() =="bas"){
                direc ="gauche";
            }
            else if(getCurrentDirection() =="gauche"){
                direc ="bas";
            }
            else if(getCurrentDirection() =="haut"){
                direc = "droite";
            }
        }
    }

    for(var i = 0; i<ice.length; i++){
        if (snake[0][0]==ice[i][0] && snake[0][1]==ice[i][1]){
            direcBloc = true;
            return false;
        }
        else {
            direcBloc = false;
        }
    }

    return false;
}

//Direction du serpent
function getCurrentDirection(){
    if (snake[0][0] > snake[1][0] && snake[0][1] == snake[1][1])
        return "droite";
    if (snake[0][0] < snake[1][0] && snake[0][1] == snake[1][1])
        return "gauche";
    if (snake[0][0] == snake[1][0] && snake[0][1] < snake[1][1])
        return "haut";
    if (snake[0][0] == snake[1][0] && snake[0][1] > snake[1][1])
        return "bas"; 
}

function direction(event){
    if (direcBloc == false){
        if(event.keyCode==37 && getCurrentDirection() !="droite"){
            direc ="gauche";
        }
        else if(event.keyCode==38 && getCurrentDirection() !="bas"){
            direc ="haut";
        }
        else if(event.keyCode==39 && getCurrentDirection() !="gauche"){
            direc ="droite";
        }
        else if(event.keyCode==40 && getCurrentDirection() !="haut"){
            direc = "bas";
        }
        if(event.keyCode==32 && isGame == true){
            if (pause==false){
                clearInterval(game);
                pause = true;
            }
            else {
                game = setInterval(playGame,delay);
                pause = false;
            }
                
        }
    }
}

function loadLevel(numLvl){
    var url = "./level"+numLvl+".json";
    var req = new XMLHttpRequest();
    req.open("GET", url);

    req.onerror = function() { console.log("Échec de chargement "+url); };

    req.onload = function() {
        if (req.status === 200) {
            var json = JSON.parse(req.responseText);
            tailleGrille = json.tailleGrille;
            delay = json.delay;
            walls = json.walls;
            food = json.food;
            snake = json.snake;
            ice = json.ice;
            oil = json.oil;
            clearInterval(game); // clear l'intervalle game pour le delai
            startGame();
            isGame = true;
        } else {
            console.log("Erreur " + req.status);
        }
    };
    req.send();
}

function loadMenu(){// charge le json menu et puis démare le menu
    var url = "./menu.json";
    var req = new XMLHttpRequest();
    req.open("GET", url);
    req.onerror = function() { console.log("Échec de chargement "+url); };
    req.onload = function() {
        if (req.status === 200) {
            var json = JSON.parse(req.responseText);
            tailleGrille = json.tailleGrille;
            walls = json.menu;
            snake = json.snake;
            levels = json.levels;
            startMenu();
        } else {
            console.log("Erreur " + req.status);
        }
    };
    req.send();
}

function startMenu(){//démarre le menu
    clearInterval(snakeMenuInterval);
    placeCanvas();
    snakeMenuInterval = setInterval(drawSnakeMenu,100);
    drawWall();
    soundGame(118,58,2);
    if (startMenuFirstTime == false){
        document.addEventListener("mousedown", e => {
            rect = canvas.getBoundingClientRect();
            var X = e.clientX - rect.left-10;
            var Y = e.clientY - rect.top-10;
            choiceLevel(X, Y);
           
        });
        startMenuFirstTime = true;
    }
    
}

function drawSnakeMenu(){
    //permet de faire tourner le serpent autour du titre
    if (snake[0][0] == 30 && snake[0][1] == 2){
        direc = "droite";
    }
    else if(snake[0][0] == 90 && snake[0][1] == 2){
        direc = "bas";
    }
    else if(snake[0][0] == 90 && snake[0][1] == 25){
        direc = "gauche";
    }
    else if(snake[0][0] == 30 && snake[0][1] == 25){
        direc = "haut";
    }
    
    drawSnake();
    moveSnake();
    drawSong(tailleGrille[0]-2,tailleGrille[1]-2,2);
    choiceLevel(snake[0][0]*tailleCaseEnPx, snake[0][1]*tailleCaseEnPx);
    
}

function choiceLevel(X, Y){
    var x = Math.floor(X/tailleCaseEnPx);
    var y = Math.floor(Y/tailleCaseEnPx);
    for(var i=0; i<levels.length; i++){
        if (x>=levels[i][0][0] && y>=levels[i][0][1] && x<=levels[i][1][0] && y<=levels[i][1][1]){
            clearInterval(snakeMenuInterval);
            loadLevel(i+1);
            direc = null;
        }
    }
}

function loadGameOver(){
    var url = "./endGameRejoue.json";
    var req = new XMLHttpRequest();
    req.open("GET", url);
    req.onerror = function() { console.log("Échec de chargement "+url); };
    req.onload = function() {
        if (req.status === 200) {
            var json = JSON.parse(req.responseText);
            tailleGrille = [60,30];
            walls = json.menu;
            startGameOver();
            currentMusic = false;
            gameOverStatut=true;
            isGame=false;
            
        } else {
            console.log("Erreur " + req.status);
        }
    };
    req.send();
}

function startGameOver(){
    placeCanvas();
    drawWall();
    drawScore();
    document.addEventListener("mousedown", e => {
        rect = canvas.getBoundingClientRect();
        var X = e.clientX - rect.left-10;
        var Y = e.clientY - rect.top-10;
        if (gameOverStatut==true)
            choiceRestart(X, Y);
    });
}

function choiceRestart(X, Y){
    var x = Math.floor(X/tailleCaseEnPx);
    var y = Math.floor(Y/tailleCaseEnPx);
    if (x>=8 && y>=14 && x<=50 && y<=22){
        loadMenu();
        if(currentMusicVolume == 1 || currentMusicVolume == 2)
            audioGame.play();
        score=0;
        gameOverStatut = false;
    }
}

function drawImage(src, x, y, taillex, tailley) {
    var img = new Image();
    img.onload = function() {
      context.drawImage(img, x, y,taillex,tailley);
      
    };
    img.src = src;
}


window.addEventListener("load", cree_abonnements);
